module.exports = {
    plugins: ['jest'],
    env: {
      commonjs: true,
      es6: true,
      node: true,
      'jest/globals': true,
    },
    extends: [
      'airbnb-base',
    ],
    globals: {
      Atomics: 'readonly',
      SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
      sourceType: 'script',
      ecmaVersion: 2020,
      ecmaFeatures: {
        impliedStrict: true,
      },
    },
    rules: {
      'no-underscore-dangle': [2, { allow: ['_id'] }],
    },
  };
  