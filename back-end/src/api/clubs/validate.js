const createError = require('http-errors');
const Promise = require('bluebird');
const { ObjectId } = require('mongodb');
const dbProvider = require('../../db');
const Validate = require('../../middlewares/validate');
const schema = require('../../models/clubs');
const {
  COLLECTIONS: { CLUBS },
} = require('../../constants');

const checkClubId = async ({ params: { _id } }) => {
  const db = await dbProvider.get();

  const count = await db
    .collection(CLUBS)
    .countDocuments({ _id: ObjectId(_id) });

  if (count === 0) {
    throw createError.NotFound(`club with id ${_id} does not exist `);
  }

  return null;
};

const checkNameUnicity = async (req) => {
  const db = await dbProvider.get();
  const count = await db
    .collection(CLUBS)
    .countDocuments({ name: req.body.name });
  if (count > 0) {
    throw createError.BadRequest(`name ${req.body.name} already exists `);
  }

  return null;
};
const checkEmailUnicity = async (req) => {
  const db = await dbProvider.get();
  const count = await db
    .collection(CLUBS)
    .countDocuments({ email: req.body.email });
  if (count > 0) {
    throw createError.BadRequest(`email ${req.body.email} already exists `);
  }

  return null;
};
module.exports = new Validate(schema, {
  create: async (req, res, next) => Promise.each([
    checkNameUnicity,
    checkEmailUnicity],
  async (fn) => fn(req, res, next)),
  update: async (req, res, next) => Promise.each(
    [
      checkClubId,
    ],
    async (fn) => fn(req, res, next),
  ),
  remove: async (req, res, next) => Promise.each(
    [
      checkClubId,
    ], async (fn) => fn(req, res, next),
  ),

});
