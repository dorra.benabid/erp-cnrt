const express = require('express');
const clubs = require('./clubs');
const members = require('./members');

module.exports = express
  .Router()
  .use('/clubs', clubs)
  .use('/members', members);
