const express = require('express');
const render = require('../../middlewares/render');
const DAO = require('../../middlewares/dao');
const validate = require('./validate');
const {
  COLLECTIONS: { MEMBERS },
} = require('../../constants');

const dao = new DAO(MEMBERS);

module.exports = express
  .Router()
  .get('/', dao.list, render.list)
  .get('/:_id', validate.get, dao.get, render.get)
  .post('/', validate.create, dao.create, render.create)
  .put('/:_id', validate.update, dao.update, render.update)
  .delete(
    '/:_id',
    validate.remove,
    dao.remove,
    render.remove,
  );
