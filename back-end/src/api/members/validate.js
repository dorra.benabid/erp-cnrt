const createError = require('http-errors');
const Promise = require('bluebird');
const { ObjectId } = require('mongodb');
const dbProvider = require('../../db');
const Validate = require('../../middlewares/validate');
const schema = require('../../models/members');
const {
  COLLECTIONS: { MEMBERS },
} = require('../../constants');

const checkMemberId = async ({ params: { _id } }) => {
  const db = await dbProvider.get();

  const count = await db
    .collection(MEMBERS)
    .countDocuments({ _id: ObjectId(_id) });

  if (count === 0) {
    throw createError.NotFound(`member with id ${_id} does not exist `);
  }

  return null;
};

const checkEmailUnicity = async (req) => {
  const db = await dbProvider.get();
  const count = await db
    .collection(MEMBERS)
    .countDocuments({ email: req.body.email });
  if (count > 0) {
    throw createError.BadRequest(`email ${req.body.email} already exists `);
  }

  return null;
};
module.exports = new Validate(schema, {
  create: async (req, res, next) => Promise.each([
    checkEmailUnicity],
  async (fn) => fn(req, res, next)),
  update: async (req, res, next) => Promise.each(
    [
      checkMemberId,
    ],
    async (fn) => fn(req, res, next),
  ),
  remove: async (req, res, next) => Promise.each(
    [
      checkMemberId,
    ], async (fn) => fn(req, res, next),
  ),

});
