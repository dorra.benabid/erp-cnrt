/* eslint-disable global-require */
const express = require('express');
require('express-async-errors');

const app = express();
const http = require('http');
const expressPino = require('express-pino-logger');
const pino = require('pino');
const expressProm = require('express-prom-bundle');
const cors = require('cors');
const fileupload = require('express-fileupload');
const config = require('./config');
const dbProvider = require('./db');
const errorHandler = require('./middlewares/errorHandler');
const swagger = require('./middlewares/swagger');

const logger = pino({ level: config.LOG_LEVEL });
const server = http.createServer(app);

if (process.env.NODE_ENV !== 'production') {
  swagger(app);
}

app
  .use(expressProm({ includeMethod: true }))
  .use(expressPino({ logger }))
  .use(fileupload({ useTempFiles: true }))
  .use(express.json({ limit: '50mb' }))
  .use(cors())
  .use(dbProvider.resolve)
  .use('/api/', require('./api'))
  .use(errorHandler);

server.listen(config.PORT, config.HOST, () => logger.info(`Listening on ${config.HOST}:${config.PORT}`));

['SIGINT', 'SIGTERM'].forEach((signal) => {
  process.once(signal, async () => {
    await server.close();
    process.exit(1);
  });
});

module.exports = {
  app,
  server,
};
