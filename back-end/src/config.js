const CONFIG = {
  LOG_LEVEL: process.env.LOG_LEVEL || 'info',
  HOST: process.env.HOST || '0.0.0.0',
  PORT: process.env.PORT || 8080,
  DB_URL_TEMPLATE: process.env.DB_URL_TEMPLATE || 'mongodb://localhost:27017',
  DB_NAME_TEMPLATE: process.env.DB_NAME_TEMPLATE || 'erp-rotaract',
  ENVIRONMENT: process.env.ENVIRONMENT || 'dev',
  FRONT_PATH: process.env.FRONT_PATH || 'http://localhost:3000',

};

module.exports = CONFIG;
