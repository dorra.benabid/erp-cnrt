module.exports = {
  COLLECTIONS: {
    CLUBS: 'clubs',
    MEMBERS: 'members',
  },
  GENDER: {
    MEN: 'MEN',
    WOMEN: 'WOMEN',
  },
  COORDINATION_STATUS: {
    STATUS1: 'STATUS1',
    STATUS2: 'STATUS2',
  },
  CLUB_STATUS: {
    STATUS1: 'STATUS1',
    STATUS2: 'STATUS2',
  },
  DEFAULT_IDENTITY_PHOTO: 'https://cdn-icons-png.flaticon.com/512/20/20079.png',
};
