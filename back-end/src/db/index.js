const { MongoClient } = require('mongodb');
const _ = require('lodash');
const Promise = require('bluebird');

const { DB_URL_TEMPLATE, DB_NAME_TEMPLATE } = require('../config');

const pool = {};

exports.connect = async () => {
  const client = await MongoClient.connect(
    `${DB_URL_TEMPLATE}/${DB_NAME_TEMPLATE}`,
  );

  return client;
};

exports.resolve = async (req, res, next) => {
  await exports.connect();

  return next();
};

exports.get = async () => {
  const name = _.template(DB_NAME_TEMPLATE)();
  const client = await exports.connect();
  return client.db(name);
};

exports.close = async () => Promise.map(_.values(pool), (handler) => handler.close());
