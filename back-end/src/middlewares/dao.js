/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */

const _ = require('lodash');
const createError = require('http-errors');
const { ObjectId } = require('mongodb');
const pluralize = require('pluralize');
const dbProvider = require('../db');

module.exports = function (
  Model,
  { alias } = {
    alias: Model,
  },
) {
  if (!Model) {
    throw new Error('Model is required!');
  }

  this.create = async (req, res, next) => {
    req.log.info(`Initiating create operation for ${Model}..`);

    const db = await dbProvider.get();

    const { refs, virtuals } = require(`../models/${Model.toLowerCase()}`);

    const doc = _.chain(req.body)
      .omit(virtuals || [])
      .merge({
        ..._.chain(refs)
          .map((ref) => [
            ref,
            !_.isUndefined(req.body[ref]) ? ObjectId(req.body[ref]) : null,
          ])
          .fromPairs()
          .omitBy(_.isNil)
          .value(),
        createdAt: new Date(),
      })
      .value();

    const {
      ops: { 0: inserted },
    } = await db
      .collection(pluralize(Model))
      .insertOne(doc)
      .catch((err) => next(err));

    req.model = inserted;
    req.virtuals = _.pick(req.body, virtuals || []);

    return next();
  };

  this.update = async (req, res, next) => {
    req.log.info(`Initiating update operation for ${Model}..`);
    const { _id } = req.params;
    const db = await dbProvider.get();

    const exists = await db.collection(pluralize(Model)).countDocuments({
      _id: ObjectId(_id),
    });

    if (!exists) {
      throw createError.NotFound(
        `${alias} with id ${req.params._id} not Found`,
      );
    }

    const { refs, virtuals } = require(`../models/${Model.toLowerCase()}`);

    const payload = _.chain(req.body)
      .omit(virtuals || [])
      .merge({
        ..._.chain(refs)
          .map((ref) => [
            ref,
            !_.isUndefined(req.body[ref]) ? ObjectId(req.body[ref]) : null,
          ])
          .fromPairs()
          .omitBy(_.isNil)
          .value(),
        createdAt: new Date(),
      })
      .value();

    const { value: doc } = await db
      .collection(pluralize(Model))
      .findOneAndUpdate(
        {
          _id: ObjectId(_id),
        },
        {
          $set: payload,
        },
        {
          returnOriginal: false,
        },
      )
      .catch((err) => next(err));

    // req.virtuals = _.pick(req.body, virtuals || []);
    req.model = doc;
    next();
  };

  this.remove = async (req, res, next) => {
    req.log.info(`Initiating delete operation for ${Model}..`);
    const db = await dbProvider.get();

    const exists = await db.collection(pluralize(Model)).countDocuments({
      _id: ObjectId(req.params._id),
    });

    if (!exists) {
      throw createError.NotFound('Not Found');
    }

    await db.collection(pluralize(Model)).deleteOne({
      _id: ObjectId(req.params._id),
    });

    return next();
  };

  this.get = async (req, res, next) => {
    req.log.info(`Initiating get operation for ${Model}..`);
    const { _id } = req.params;

    const db = await dbProvider.get();

    const conditions = req.query.conditions || {};
    const projection = req.query.select || {};

    const query = db.collection(pluralize(Model)).findOne(
      {
        ...conditions,
        _id: ObjectId(_id),
      },
      {
        projection,
      },
    );

    req.model = await query;

    if (!req.model) {
      throw createError.NotFound(`${alias} with id ${_id} does not exist`);
    }
    return next();
  };

  this.list = async (req, res, next) => {
    req.log.info(`Initiating list operation for ${Model}..`);
    const db = await dbProvider.get();

    const skip = _.parseInt(req.query.skip) || 0;
    const limit = _.parseInt(req.query.limit);
    const conditions = req.query.conditions || {};
    const { sort } = req.query;
    const { select } = req.query;

    let query = db.collection(pluralize(Model)).find(conditions);

    if (skip) query = query.skip(skip);
    if (limit) query = query.limit(limit);
    if (sort) query = query.sort(sort).collation({ locale: 'en' });
    if (select) {
      query = query.project(
        _(select)
          .map((it) => [it, 1])
          .fromPairs()
          .value(),
      );
    }

    req.models = await query.toArray();

    return next();
  };

  this.count = async (req, res, next) => {
    req.log.info(`Initiating list operation for ${Model}..`);
    const db = await dbProvider.get();
    const conditions = req.query.conditions || {};

    req.count = await db
      .collection(pluralize(Model))
      .countDocuments(conditions);

    return next();
  };

  this.upsert = async (req, res, next) => {
    const { computed } = require(`../models/${Model.toLowerCase()}`);

    req.body = _.merge(
      req.body,
      _.chain(computed || [])
        .map((fn, field) => [field, fn(req.body)])
        .fromPairs()
        .omitBy(_.isEmpty)
        .value(),
    );

    const db = await dbProvider.get();

    const entity = await db.collection(pluralize(Model)).findOne(
      {
        $or: _.compact([
          req.params._id ? { _id: ObjectId(req.params._id) } : null,
          req.query.conditions,
        ]),
      },
      { projection: { _id: true } },
    );

    const _id = _.get(entity, '_id');

    if (_id) {
      req.params._id = _id;
      return this.update(req, res, next);
    }

    return this.create(req, res, next);
  };
};
