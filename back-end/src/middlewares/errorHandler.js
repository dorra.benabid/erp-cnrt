const pino = require('pino');
const config = require('../config');

const logger = pino({ level: config.LOG_LEVEL });

module.exports = (err, req, res, next) => {
  logger.error(err);
  if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
    res.status(400).json({ message: 'Malformed JSON' });
  } else {
    res.status(err.statusCode || 500);
    res.send(err);
  }
  next();
};
