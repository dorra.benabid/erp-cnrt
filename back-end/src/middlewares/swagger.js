const swaggerGenerator = require('express-swagger-generator');
const config = require('../config');

module.exports = (app) => {
  const expressSwagger = swaggerGenerator(app);
  const options = {
    swaggerDefinition: {
      info: {
        description: 'API Spec',
        title: 'Swagger',
        version: '1.0.0',
      },
      host: `${config.HOST}:${config.PORT}`,
      produces: ['application/json'],
      schemes: ['http', 'https'],
    },
    basedir: __dirname,
    files: ['../api/**/*.js', '../payloads/*.js', '../models/*.js'],
  };

  expressSwagger(options);
};
