const _ = require('lodash');
const { isValid } = require('mongodb').ObjectId;
const Joi = require('joi');
const createError = require('http-errors');

module.exports = function (
  { schema },
  {
    get = _.noop,
    create = _.noop,
    update = _.noop,
    patch = _.noop,
    remove = _.noop,
    list = _.noop,
    extras = {},
  } = {},
) {
  if (
    _.chain(schema)
      .some((it) => it instanceof Joi.constructor === false)
      .size()
      .value()
  ) {
    throw new Error('Invalid or missing schema');
  }
  this.extras = extras;

  this.create = async (req, res, next) => {
    req.log.info(`Validating create payload for ${req.originalUrl}..`);

    const { error } = schema.create.validate(req.body);

    if (error) {
      throw createError.BadRequest(error.message);
    }

    if (_.isFunction(create)) {
      try {
        await create(req, res, next);
      } catch (err) {
        req.log.warn(`Failed to validate payload for ${req.originalUrl}..`);

        throw err;
      }
    }

    return next();
  };

  this.update = async (req, res, next) => {
    req.log.info(`Validating update payload for ${req.originalUrl}..`);

    if (req.params._id && !isValid(req.params._id)) {
      throw createError.NotFound(`Invalid id : ${req.params._id}`);
    }

    const { error } = schema.update.validate(req.body);

    if (error) {
      throw createError.BadRequest(error.message);
    }

    if (_.isFunction(update)) {
      try {
        await update(req, res, next);
      } catch (err) {
        req.log.warn(`Failed to validate payload for ${req.originalUrl}..`);
        return next(err);
      }
    }

    return next();
  };

  this.patch = async (req, res, next) => {
    req.log.info(`Validating patch payload for ${req.originalUrl}..`);

    if (req.params._id && !isValid(req.params._id)) {
      throw createError.NotFound(`Invalid id : ${req.params._id}`);
    }

    const { error } = schema.patch.validate(req.body);

    if (error) {
      throw createError.BadRequest(error.message);
    }

    if (_.isFunction(patch)) {
      try {
        await patch(req, res, next);
      } catch (err) {
        req.log.warn(`Failed to validate payload for ${req.originalUrl}..`);
        return next(err);
      }
    }

    return next();
  };

  this.remove = async (req, res, next) => {
    req.log.info(`Validating delete payload for ${req.originalUrl}..`);

    if (req.params._id && !isValid(req.params._id)) {
      throw createError.NotFound(`Invalid id : ${req.params._id}`);
    }

    if (_.isFunction(remove)) {
      try {
        await remove(req, res, next);
      } catch (err) {
        req.log.warn(`Failed to validate payload for ${req.originalUrl}..`);
        throw err;
      }
    }

    return next();
  };

  this.get = async (req, res, next) => {
    req.log.info(`Validating get payload for ${req.originalUrl}..`);

    if (req.params._id && !isValid(req.params._id)) {
      throw createError.NotFound(`Invalid id : ${req.params._id}`);
    }

    if (_.isFunction(get)) {
      try {
        await get(req, res, next);
      } catch (err) {
        req.log.warn(`Failed to validate payload for ${req.originalUrl}..`);
        throw err;
      }
    }

    return next();
  };

  this.list = async (req, res, next) => {
    req.log.info(`Validating get payload for ${req.originalUrl}..`);

    if (_.isFunction(list)) {
      try {
        await list(req, res, next);
      } catch (err) {
        req.log.warn(`Failed to validate payload for ${req.originalUrl}..`);
        throw err;
      }
    }

    return next();
  };
};
