const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const {
  COLLECTIONS: { CLUBS },
} = require('../constants');

const clubSchema = {
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  adress: Joi.string().required(),
  city: Joi.string().required(),
  facebookLink: Joi.string().uri(),
  instagramLink: Joi.string().uri(),
  linkedinLink: Joi.string().uri(),
  charter: Joi.string().uri(),
  charterDateObtaining: Joi.date(),
};

module.exports = {
  schema: {
    create: Joi.object(clubSchema),
    update: Joi.object(clubSchema),
  },
  collection: CLUBS,
};
