const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const passwordComplexity = require('joi-password-complexity');
const _ = require('lodash');
const {
  COLLECTIONS: { MEMBERS }, GENDER, COORDINATION_STATUS, CLUB_STATUS, DEFAULT_IDENTITY_PHOTO,
} = require('../constants');

const memberSchema = {
  email: Joi.string().email().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  gender: Joi.string()
    .valid(..._.values(GENDER))
    .required(),
  birthDate: Joi.date().required(),
  nationalIdNumber: Joi.string()
    .regex(/^[0-9]{8}$/)
    .required(),
  phoneNumber: Joi.string()
    .regex(/^[0-9]{8}$/)
    .required(),
  address: Joi.string(),
  isInducted: Joi.boolean().required(),
  intronisationDate: Joi.date().required(),
  currentCoordinationStatus: Joi
    .string()
    .valid(..._.values(COORDINATION_STATUS))
    .required(),
  currentClubStatus: Joi
    .string()
    .valid(..._.values(CLUB_STATUS))
    .required(),
  entryClubDate: Joi.date().required(),
  identityPhoto: Joi.link().default(DEFAULT_IDENTITY_PHOTO),
  password: passwordComplexity({
    min: 8,
    max: 50,
    lowerCase: 1,
    upperCase: 1,
    numeric: 1,
  }),
  enabled: Joi.boolean().default(false),
};

module.exports = {
  schema: {
    create: Joi.object(memberSchema),
    update: Joi.object(memberSchema),
  },
  collection: MEMBERS,
};
